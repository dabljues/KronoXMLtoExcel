﻿using System;

namespace KronoXMLtoExcel
{
    public class DuplicateInvoice
    {
//        public string InvoiceNumber;
//        public List<string> FileNames;
//        public List<string> Mrn;
        public string FileName;

        public string Mrn;

        public DuplicateInvoice()
        {
//            FileNames = new List<string>();
//            Mrn = new List<string>();
        }

        public DuplicateInvoice(string fileName, string mrn)
        {
            FileName = fileName ?? throw new ArgumentNullException(nameof(fileName));
            Mrn = mrn ?? throw new ArgumentNullException(nameof(mrn));
        }
    }
}