﻿using System;
using System.Collections.Generic;
using System.IO;
using SpreadsheetLight;

namespace KronoXMLtoExcel
{
    public class ExcelFile
    {
        private readonly int _fontSizeInfo = 9;

        private readonly Dictionary<int, string> _polishMonths = new Dictionary<int, string>
        {
            {1, "STYCZEŃ"},
            {2, "LUTY"},
            {3, "MARZEC"},
            {4, "KWIECIEŃ"},
            {5, "MAJ"},
            {6, "CZERWIEC"},
            {7, "LIPIEC"},
            {8, "SIERPIEŃ"},
            {9, "WRZESIEŃ"},
            {10, "PAŹDZIERNIK"},
            {11, "LISTOPAD"},
            {12, "GRUDZIEŃ"}
        };

//        private int _fontSizeTable = 11;
        private readonly SLDocument _sl;

        private readonly string _templateName = "szablon.xlsx";
        private readonly string _font = "Calibri";

        public ExcelFile()
        {
            _sl = new SLDocument(_templateName);
        }

        public void WriteFile(string xmlPath)
        {
            var lp = 1;
            var rowIndex = 9;
            var files = Directory.GetFiles(xmlPath);
            foreach (var file in files)
            {
                var xmlFile = new XmlFile(file);
                xmlFile.ReadFile();
                // Lp.
                _sl.SetCellValue(rowIndex, 1, lp);
                // Data
                _sl.SetCellValue(rowIndex, 2, xmlFile.Date);
                // MRN
                _sl.SetCellValue(rowIndex, 3, xmlFile.Mrn);
                // UC Wywozu
                _sl.SetCellValue(rowIndex, 4, xmlFile.Uc);
                // Kod
                _sl.SetCellValue(rowIndex, 5, xmlFile.Code);
                // Towar
                _sl.SetCellValue(rowIndex, 6, xmlFile.Type);
                // Faktura
                for (var colIndex = 1; colIndex < 7; colIndex++)
                    _sl.MergeWorksheetCells(rowIndex, colIndex, rowIndex + xmlFile.InvoiceNumbers.Count - 1, colIndex);
                foreach (var invoiceNumber in xmlFile.InvoiceNumbers)
                {
                    _sl.SetCellValue(rowIndex, 7, invoiceNumber);
                    rowIndex++;
                }
                lp++;
            }
            var style = _sl.CreateStyle();
            style.SetFont(_font, _fontSizeInfo);
            style.Font.Bold = true;
            var firstCell =
                $"Umowa z dnia {DateTime.Today.AddMonths(-1):dd.MM.yyyy} z Malta - Decor Sp.z o.o. (obsługa celna)";
            var secondCell =
                $"Specyfikacja do faktury za {_polishMonths[DateTime.Today.AddMonths(-1).Month]} {DateTime.Today.AddMonths(-1).Year} - {lp-1} zgłoszeń celnych eksportowych x 75,00 PLN  = {(lp - 1) * 75.0:F2} PLN";
            _sl.SetCellValue(4, 1, firstCell);
            _sl.SetCellValue(5, 1, secondCell);
            _sl.SetCellStyle(4, 1, style);
            _sl.SetCellStyle(5, 1, style);
            _sl.SaveAs("new.xlsx");
        }
    }
}