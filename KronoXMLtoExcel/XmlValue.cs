﻿namespace KronoXMLtoExcel
{
    public class XmlValue
    {
        public XmlValue(bool correct, string value)
        {
            IsCorrect = correct;
            Value = value;
        }

        public int UniqueId { get; set; }

        // Property określa poprawność wartości XML
        public bool IsCorrect { get; set; }

        // Property zawiera wartość XML
        public string Value { get; set; }
    }
}