﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace KronoXMLtoExcel
{
    public partial class FormMain : Form
    {
        // Nazwa pliku konfiguracyjnego (ostatnia wybrana ścieżka i nazwa umowy)
        private const string ConfigFile = "konfiguracja.txt";

        // Obiekt do czytania pliku konfiguracyjnego
        private readonly ConfigReader _configReader = new ConfigReader();

        private readonly Dictionary<int, XmlFile.XmlValueType> _columnTypes = new Dictionary<int, XmlFile.XmlValueType>
        {
            {-1, XmlFile.XmlValueType.UniqueId},
            {0, XmlFile.XmlValueType.Name},
            {1, XmlFile.XmlValueType.Date},
            {2, XmlFile.XmlValueType.Mrn},
            {3, XmlFile.XmlValueType.Uc},
            {4, XmlFile.XmlValueType.Code},
            {5, XmlFile.XmlValueType.Desc},
            {6, XmlFile.XmlValueType.Invoice}
        };

        // 4 DataTable, z których każda jest DataSourcem jednego z 4 dGV
        private readonly DataTable _dt = new DataTable();

        private readonly DataTable _dtRejected = new DataTable();

        private readonly DataTable _dtDuplicateInvoices = new DataTable();

        private DataTable _dtUnread = new DataTable();

        // Lista koordynatów w tabeli do wartości, których nie udało się przeczytać z XMLa
        private readonly List<CellCoordinates> _wrongCells = new List<CellCoordinates>();

        // Lista wartości pól zinterpretowanycho jak złe
        private readonly List<XmlFile> _wrongRecords = new List<XmlFile>();

        // Obiekt do obsługiwania pliku Excela
        private readonly ExcelFileHandler _excelFileHandler;

        // Ścieżka do plików XML
        private string _xmlPath = string.Empty;

        // Boole wspomagające event DataBinded (pierwszy zmienia się na false po wygenerowaniu danych,
        // drugi zaś po PIERWSZEJ zmianie zakładki na
        private bool _firstLoad = true;

        private bool _firstTab = true;

        // Bool mówiący o tym, czy generować PDFa, czy nie
        private bool _generatePdf;

        public FormMain()
        {
            InitializeComponent();
            progressBarGenerate.Enabled = false;
            _excelFileHandler = new ExcelFileHandler();
            SetUpStatisticsDgv();
            SetUpRejectedBy514Dgv();
            SetUpUnreadFilesDgv();
            SetUpDuplicateInvoicesDgv();
            SetUpContractName();
        }

        /// <summary>
        ///     Funkcja wczytuje nazwę umowy z pliku konfiguracyjnego
        /// </summary>
        private void SetUpContractName()
        {
            textBoxContractName.Text = _configReader.GetContractName(Path.GetFullPath(ConfigFile));
        }

        /// <summary>
        ///     Funkcja inicjalizuje dGV ze statystykami ogólnymi
        /// </summary>
        private void SetUpStatisticsDgv()
        {
            var column = new DataColumn
            {
                DataType = Type.GetType("System.Int32"),
                ColumnName = "Ilość plików ze złym NIPem",
                Caption = "BadNip",
                ReadOnly = true
            };
            _dt.Columns.Add(column);
            column = new DataColumn
            {
                DataType = Type.GetType("System.Int32"),
                ColumnName = "Ilość plików odrzuconych przez IE514",
                Caption = "RejectedBy514",
                ReadOnly = true
            };
            _dt.Columns.Add(column);
            column = new DataColumn
            {
                DataType = Type.GetType("System.Int32"),
                ColumnName = "Ilość plików, których nie udało się przeczytać",
                Caption = "UnreadFiles",
                ReadOnly = true
            };
            _dt.Columns.Add(column);
            dataGridViewStatistics.AllowUserToAddRows = false;
        }

        /// <summary>
        ///     Funkcja inicjalizuje dGV z plikami odrzuconymi przez pliki IE514
        /// </summary>
        private void SetUpRejectedBy514Dgv()
        {
            var column = new DataColumn
            {
                DataType = Type.GetType("System.String"),
                ColumnName = "MRN",
                Caption = "MRN"
            };
            _dtRejected.Columns.Add(column);
            column = new DataColumn
            {
                DataType = Type.GetType("System.String"),
                ColumnName = "Nazwa pliku",
                Caption = "FileName"
            };
            _dtRejected.Columns.Add(column);
            dataGridViewRejectedBy514.AllowUserToAddRows = false;
        }

        /// <summary>
        ///     Funkcja inicjalizuje dGV z plikami z błędnymi wartościami
        /// </summary>
        private void SetUpUnreadFilesDgv()
        {
            var column = new DataColumn
            {
                DataType = Type.GetType("System.String"),
                ColumnName = "Nazwa pliku",
                Caption = "FileName",
                ReadOnly = false
            };
            _dtUnread.Columns.Add(column);
            column = new DataColumn
            {
                DataType = Type.GetType("System.String"),
                ColumnName = "Data",
                Caption = "Date",
                ReadOnly = false
            };
            _dtUnread.Columns.Add(column);
            column = new DataColumn
            {
                DataType = Type.GetType("System.String"),
                ColumnName = "MRN",
                Caption = "MRN",
                ReadOnly = false
            };
            _dtUnread.Columns.Add(column);
            column = new DataColumn
            {
                DataType = Type.GetType("System.String"),
                ColumnName = "UCWywozu",
                Caption = "UCWywozu",
                ReadOnly = false
            };
            _dtUnread.Columns.Add(column);
            column = new DataColumn
            {
                DataType = Type.GetType("System.String"),
                ColumnName = "Kod",
                Caption = "Code",
                ReadOnly = false
            };
            _dtUnread.Columns.Add(column);
            column = new DataColumn
            {
                DataType = Type.GetType("System.String"),
                ColumnName = "Towar",
                Caption = "Desc",
                ReadOnly = false
            };
            _dtUnread.Columns.Add(column);
            column = new DataColumn
            {
                DataType = Type.GetType("System.String"),
                ColumnName = "Faktura",
                Caption = "Invoice",
                ReadOnly = false
            };
            _dtUnread.Columns.Add(column);
            dataGridViewUnreadFiles.AllowUserToAddRows = false;
            dataGridViewUnreadFiles.AllowUserToResizeColumns = true;
            _firstLoad = true;
            _firstTab = true;
        }

        /// <summary>
        ///     Funkcja inicjalizuje dGV z duplikatami faktur
        /// </summary>
        private void SetUpDuplicateInvoicesDgv()
        {
            var column = new DataColumn
            {
                DataType = Type.GetType("System.String"),
                ColumnName = "Numer faktury",
                Caption = "InvoiceNumber",
                ReadOnly = false
            };
            _dtDuplicateInvoices.Columns.Add(column);
//            column = new DataColumn
//            {
//                DataType = Type.GetType("System.String"),
//                ColumnName = "Nazwa pliku",
//                Caption = "FileName",
//                ReadOnly = false
//            };
//            _dtDuplicateInvoices.Columns.Add(column);
            column = new DataColumn
            {
                DataType = Type.GetType("System.String"),
                ColumnName = "Nazwy plików",
                Caption = "FileNames",
                ReadOnly = false
            };
            _dtDuplicateInvoices.Columns.Add(column);
            column = new DataColumn
            {
                DataType = Type.GetType("System.String"),
                ColumnName = "MRN",
                Caption = "MRN",
                ReadOnly = false
            };
            _dtDuplicateInvoices.Columns.Add(column);
            dataGridViewDuplicateInvoices.AllowUserToAddRows = false;
        }

        /// <summary>
        ///     Funkcja czyści, wstawia kolumny do DataTable UnreadFiles, aby uniknąć undefined behaviour jeśli chodzi o eventy
        /// </summary>
        private void CleanUnreadFilesDataTable()
        {
            _dtUnread = new DataTable();
            SetUpUnreadFilesDgv();
        }

        /// <summary>
        ///     Funkcja wypełnia tabelę z ogólnymi statystykami
        /// </summary>
        /// <param name="stats"></param>
        private void AddGeneralStatistics(Statistics stats)
        {
            // Generowanie ogólnych statystyk
            var row = _dt.NewRow();
            row["Ilość plików ze złym NIPem"] = stats.BadNipFiles;
            row["Ilość plików odrzuconych przez IE514"] = stats.RejectedBy514Files;
            row["Ilość plików, których nie udało się przeczytać"] = stats.UnreadFiles;
            _dt.Rows.Clear();
            _dt.Rows.Add(row);
            dataGridViewStatistics.DataSource = _dt;
        }

        /// <summary>
        ///     Funkcja wypełnia tabelę z plikami, które zostały odrzucone przez pliki IE514
        /// </summary>
        /// <param name="stats"></param>
        private void AddRejectedStatistics(Statistics stats)
        {
            // Generowanie statystyk na temat MRN
            DataRow row;
            _dtRejected.Rows.Clear();
            foreach (var rejected in stats.RejectedFilesInfo)
            {
                row = _dtRejected.NewRow();
                row["MRN"] = rejected.Key;
                row["Nazwa pliku"] = rejected.Value;
                _dtRejected.Rows.Add(row);
            }
            dataGridViewRejectedBy514.DataSource = _dtRejected;
            dataGridViewRejectedBy514.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewRejectedBy514.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }

        /// <summary>
        ///     Funkcja wypełnia tabelę z plikami, których nie udało się przeczytać
        /// </summary>
        /// <param name="stats"></param>
        private void AddUnreadFilesStatistics(Statistics stats)
        {
            DataRow row;
            CleanUnreadFilesDataTable();
            _wrongCells.Clear();
            _wrongRecords.Clear();
            var rowIndex = 0;
            // Dla każdego pliku, którego nie udało się przeczytać tworzymy osobny wiersz w tabeli
            // , a do tego dla każdej źle przeczytanej informacji (MRN, UC itd.) tworzymy jej koordynaty
            // do listy (rowIndex, colIndex), aby potem pokolorować taką komórkę w tabeli na czerwono
            foreach (var unreadFile in stats.UnreadFilesInfo)
            {
                var xmlFile = new XmlFile(unreadFile[XmlFile.XmlValueType.UniqueId].First().Value);
                var colIndex = -1;
                row = _dtUnread.NewRow();
                var value = unreadFile[XmlFile.XmlValueType.Name].First().Value;
                row["Nazwa pliku"] = value;
                xmlFile.FileName = value;
                if (!unreadFile[XmlFile.XmlValueType.Date].First().IsCorrect)
                    _wrongCells.Add(new CellCoordinates(rowIndex, 1));
                value = unreadFile[XmlFile.XmlValueType.Date].First().Value;
                row["Data"] = value;
                xmlFile.StringDate = value;
                if (!unreadFile[XmlFile.XmlValueType.Mrn].First().IsCorrect)
                    _wrongCells.Add(new CellCoordinates(rowIndex, 2));
                value = unreadFile[XmlFile.XmlValueType.Mrn].First().Value;
                row["MRN"] = value;
                xmlFile.Mrn = value;
                if (!unreadFile[XmlFile.XmlValueType.Uc].First().IsCorrect)
                    _wrongCells.Add(new CellCoordinates(rowIndex, 3));
                value = unreadFile[XmlFile.XmlValueType.Uc].First().Value;
                row["UCWywozu"] = value;
                xmlFile.Uc = value;
                if (!unreadFile[XmlFile.XmlValueType.Code].First().IsCorrect)
                    _wrongCells.Add(new CellCoordinates(rowIndex, 4));
                value = unreadFile[XmlFile.XmlValueType.Code].First().Value;
                row["Kod"] = value;
                xmlFile.Code = value;
                if (!unreadFile[XmlFile.XmlValueType.Desc].First().IsCorrect)
                    _wrongCells.Add(new CellCoordinates(rowIndex, 5));
                value = unreadFile[XmlFile.XmlValueType.Desc].First().Value;
                row["Towar"] = value;
                xmlFile.Type = value;
                foreach (var invoice in unreadFile[XmlFile.XmlValueType.Invoice])
                {
                    if (!invoice.IsCorrect)
                        colIndex = 6;
                    value = invoice.Value;
                    row["Faktura"] += $"[{value}]" + Environment.NewLine;
                    xmlFile.InvoiceNumbers.Add(value);
                }
                if (colIndex == 6) _wrongCells.Add(new CellCoordinates(rowIndex, colIndex));
                row["Faktura"] = row["Faktura"].ToString().TrimEnd(Environment.NewLine.ToCharArray());
                _dtUnread.Rows.Add(row);
                xmlFile.RowIndexInTable = rowIndex;
                _wrongRecords.Add(xmlFile);
                rowIndex++;
            }
            // Bindowanie danych do dGV
            dataGridViewUnreadFiles.DataSource = _dtUnread;
            // Ustalanie formatu komórek
            dataGridViewUnreadFiles.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewUnreadFiles.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewUnreadFiles.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewUnreadFiles.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewUnreadFiles.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewUnreadFiles.Columns[5].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewUnreadFiles.Columns[6].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            // Te 2 linijki umożliwiają multiline w komórkach (np. dla komórek z numerami faktur)
            dataGridViewUnreadFiles.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            dataGridViewUnreadFiles.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
        }

        /// <summary>
        ///     Funkcja wypełnia tabelę z duplikatami faktur
        /// </summary>
        private void AddDuplicateInvoicesStatistics(
            IEnumerable<KeyValuePair<string, List<DuplicateInvoice>>> duplicates)
        {
            // Generowanie statystyk na temat MRN
            _dtDuplicateInvoices.Rows.Clear();
            foreach (var duplicate in duplicates)
            {
                var row = _dtDuplicateInvoices.NewRow();
                row["Numer faktury"] = duplicate.Key;
                foreach (var duplicateInvoice in duplicate.Value)
                {
                    row["Nazwy plików"] += duplicateInvoice.FileName + Environment.NewLine;
                    row["MRN"] += duplicateInvoice.Mrn + Environment.NewLine;
                }

                row["Nazwy plików"] = row["Nazwy plików"].ToString().TrimEnd(Environment.NewLine.ToCharArray());
                row["MRN"] = row["MRN"].ToString().TrimEnd(Environment.NewLine.ToCharArray());
                _dtDuplicateInvoices.Rows.Add(row);
            }
            dataGridViewDuplicateInvoices.DataSource = _dtDuplicateInvoices;
            dataGridViewDuplicateInvoices.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewDuplicateInvoices.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewDuplicateInvoices.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewDuplicateInvoices.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            dataGridViewDuplicateInvoices.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
        }

        /// <summary>
        ///     Funkcja odpowiada za otworzenie okna do wyboru ścieżki do plików XML
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSelectXMLPath_Click(object sender, EventArgs e)
        {
            _xmlPath = _configReader.GetXmlPath(Path.GetFullPath(ConfigFile));
            File.WriteAllText(Path.GetFullPath(ConfigFile), _xmlPath);
            textBoxXMLPath.Text = _xmlPath;
        }

        /// <summary>
        ///     Funkcja odpowiada za wygenerowanie pliku Excel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonGenerateExcel_Click(object sender, EventArgs e)
        {
            Generate(false);
        }

        /// <summary>
        ///     Funkcja odpowiada za naprawienie wszystkich błędów wyświetlonych w tabeli.
        ///     Jest to zwyczajnie naniesienie zmian wprowadzonych przez użytkownika w tabeli do zestawienia
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonFixAll_Click(object sender, EventArgs e)
        {
            var goingToFix = true;
            var updates = new List<UpdateFromCell>();
            foreach (var wrongRecord in _wrongRecords)
            foreach (var value in wrongRecord.Values)
                switch (value.Key)
                {
                    case XmlFile.XmlValueType.UniqueId:
                        break;
                    case XmlFile.XmlValueType.Name:
                        break;
                    case XmlFile.XmlValueType.Date:
                    {
                        if (value.Value.First().IsCorrect) break;
                        var valueFromTable = GetUnreadDgvValue(wrongRecord.RowIndexInTable, 1);
                        var valueFromGeneration = wrongRecord.StringDate;
                        updates.Add(new UpdateFromCell(wrongRecord.RowIndexInTable, 1, valueFromTable));
                        if (valueFromTable == valueFromGeneration)
                            goingToFix = false;
                        break;
                    }
                    case XmlFile.XmlValueType.Mrn:
                    {
                        if (value.Value.First().IsCorrect) break;
                        var valueFromTable = GetUnreadDgvValue(wrongRecord.RowIndexInTable, 2);
                        var valueFromGeneration = wrongRecord.Mrn;
                        updates.Add(new UpdateFromCell(wrongRecord.RowIndexInTable, 2, valueFromTable));
                        if (valueFromTable == valueFromGeneration)
                            goingToFix = false;
                        break;
                    }
                    case XmlFile.XmlValueType.Uc:
                    {
                        if (value.Value.First().IsCorrect) break;
                        var valueFromTable = GetUnreadDgvValue(wrongRecord.RowIndexInTable, 3);
                        var valueFromGeneration = wrongRecord.Uc;
                        updates.Add(new UpdateFromCell(wrongRecord.RowIndexInTable, 3, valueFromTable));
                        if (valueFromTable == valueFromGeneration)
                            goingToFix = false;
                        break;
                    }
                    case XmlFile.XmlValueType.Code:
                    {
                        if (value.Value.First().IsCorrect) break;
                        var valueFromTable = GetUnreadDgvValue(wrongRecord.RowIndexInTable, 4);
                        var valueFromGeneration = wrongRecord.Code;
                        updates.Add(new UpdateFromCell(wrongRecord.RowIndexInTable, 4, valueFromTable));
                        if (valueFromTable == valueFromGeneration)
                            goingToFix = false;
                        break;
                    }
                    case XmlFile.XmlValueType.Desc:
                    {
                        if (value.Value.First().IsCorrect) break;
                        var valueFromTable = GetUnreadDgvValue(wrongRecord.RowIndexInTable, 5);
                        var valueFromGeneration = wrongRecord.Type;
                        updates.Add(new UpdateFromCell(wrongRecord.RowIndexInTable, 5, valueFromTable));
                        if (valueFromTable == valueFromGeneration)
                            goingToFix = false;
                        break;
                    }
                    case XmlFile.XmlValueType.Invoice:
                    {
                        if (value.Value.First().IsCorrect) break;
                        var valueFromTable = GetUnreadDgvValue(wrongRecord.RowIndexInTable, 6);
                        var invoices = GetInvoicesFromCell(valueFromTable);
                        var valueFromGeneration = wrongRecord.InvoiceNumbers;
                        updates.Add(new UpdateFromCell(wrongRecord.RowIndexInTable, 6, invoices));
                        if (invoices.SequenceEqual(valueFromGeneration))
                            goingToFix = false;
                        break;
                    }
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            if (goingToFix)
            {
                foreach (var update in updates)
                    if (update.NewValue != string.Empty)
                        UpdateRecordsInMemory(update.RowIndex(), update.ColIndex(), update.NewValue);
                    else UpdateRecordsInMemory(update.RowIndex(), update.ColIndex(), update.Invoices);
                UpdateRecordsToWrite();
//                _excelFileHandler.preparedXmlFiles.AddRange(wrongRecords);
                _excelFileHandler.CleanBeforeGeneration(true);
                foreach (var preparedFile in _excelFileHandler.PreparedXmlFiles)
                {
                    preparedFile.IsCorrectOrFixed = true;
                }
                _excelFileHandler.PreparedXmlFiles =
                    _excelFileHandler.PreparedXmlFiles.OrderBy(o => o.StringDate).ToList();
                _wrongRecords.Clear();
                Generate(true);
            }
        }

        /// <summary>
        ///     Funkcja odpowiada za naprawienie wybranego błędu w zestawieniu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonFixOne_Click(object sender, EventArgs e)
        {
            var goingToFix = true;
            var updates = new List<UpdateFromCell>();
            int selectedRowIndex;
            try
            {
                selectedRowIndex = dataGridViewUnreadFiles.SelectedRows[0].Index;
            }
            catch (ArgumentOutOfRangeException)
            {
                return;
            }
            var wrongRecord = _wrongRecords[selectedRowIndex];
            foreach (var value in wrongRecord.Values)
                switch (value.Key)
                {
                    case XmlFile.XmlValueType.UniqueId:
                    break;
                    case XmlFile.XmlValueType.Name:
                    break;
                    case XmlFile.XmlValueType.Date:
                    {
                        if (value.Value.First().IsCorrect) break;
                        var valueFromTable = GetUnreadDgvValue(wrongRecord.RowIndexInTable, 1);
                        var valueFromGeneration = wrongRecord.StringDate;
                        updates.Add(new UpdateFromCell(wrongRecord.RowIndexInTable, 1, valueFromTable));
                        if (valueFromTable == valueFromGeneration)
                            goingToFix = false;
                        break;
                    }
                    case XmlFile.XmlValueType.Mrn:
                    {
                        if (value.Value.First().IsCorrect) break;
                        var valueFromTable = GetUnreadDgvValue(wrongRecord.RowIndexInTable, 2);
                        var valueFromGeneration = wrongRecord.Mrn;
                        updates.Add(new UpdateFromCell(wrongRecord.RowIndexInTable, 2, valueFromTable));
                        if (valueFromTable == valueFromGeneration)
                            goingToFix = false;
                        break;
                    }
                    case XmlFile.XmlValueType.Uc:
                    {
                        if (value.Value.First().IsCorrect) break;
                        var valueFromTable = GetUnreadDgvValue(wrongRecord.RowIndexInTable, 3);
                        var valueFromGeneration = wrongRecord.Uc;
                        updates.Add(new UpdateFromCell(wrongRecord.RowIndexInTable, 3, valueFromTable));
                        if (valueFromTable == valueFromGeneration)
                            goingToFix = false;
                        break;
                    }
                    case XmlFile.XmlValueType.Code:
                    {
                        if (value.Value.First().IsCorrect) break;
                        var valueFromTable = GetUnreadDgvValue(wrongRecord.RowIndexInTable, 4);
                        var valueFromGeneration = wrongRecord.Code;
                        updates.Add(new UpdateFromCell(wrongRecord.RowIndexInTable, 4, valueFromTable));
                        if (valueFromTable == valueFromGeneration)
                            goingToFix = false;
                        break;
                    }
                    case XmlFile.XmlValueType.Desc:
                    {
                        if (value.Value.First().IsCorrect) break;
                        var valueFromTable = GetUnreadDgvValue(wrongRecord.RowIndexInTable, 5);
                        var valueFromGeneration = wrongRecord.Type;
                        updates.Add(new UpdateFromCell(wrongRecord.RowIndexInTable, 5, valueFromTable));
                        if (valueFromTable == valueFromGeneration)
                            goingToFix = false;
                        break;
                    }
                    case XmlFile.XmlValueType.Invoice:
                    {
                        if (value.Value.First().IsCorrect) break;
                        var valueFromTable = GetUnreadDgvValue(wrongRecord.RowIndexInTable, 6);
                        var invoices = GetInvoicesFromCell(valueFromTable);
                        var valueFromGeneration = wrongRecord.InvoiceNumbers;
                        updates.Add(new UpdateFromCell(wrongRecord.RowIndexInTable, 6, invoices));
                        if (invoices.SequenceEqual(valueFromGeneration))
                            goingToFix = false;
                        break;
                    }
                    default:
                    throw new ArgumentOutOfRangeException();
            }
            if (goingToFix)
            {
                foreach (var update in updates)
                    if (update.NewValue != string.Empty)
                        UpdateRecordsInMemory(update.RowIndex(), update.ColIndex(), update.NewValue);
                    else UpdateRecordsInMemory(update.RowIndex(), update.ColIndex(), update.Invoices);
                UpdateRecordsToWrite();
                _excelFileHandler.FixedOne(wrongRecord.UniqueId);
                var correctedFile = _excelFileHandler.PreparedXmlFiles.First(x => x.UniqueId == wrongRecord.UniqueId);
                correctedFile.IsCorrectOrFixed = true;
                _excelFileHandler.PreparedXmlFiles =
                    _excelFileHandler.PreparedXmlFiles.OrderBy(o => o.StringDate).ToList();
                _wrongRecords.RemoveAt(selectedRowIndex);
                Generate(true);
            }
        }

        /// <summary>
        ///     Funkcja odpowiada za oznaczenie złych komórek w tabeli kolorem czerwonym.
        ///     Jest to event, więc działa od razu po zbindowaniu DataSource
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewUnreadFiles_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            if (_wrongCells.Count == 0) return;
            if (_wrongRecords.Count == 0) return;
            if (_firstLoad)
            {
                _firstLoad = false;
            }
            else
            {
                if (_firstTab)
                    _firstTab = false;
                else
                    return;
            }
            var wrongRecordsIndex = 0;
            foreach (DataGridViewRow row in dataGridViewUnreadFiles.Rows)
            {
                for (var i = 0; i < 7; i++)
                {
                    var valueFromTable = row.Cells[i].Value.ToString();
                    _wrongRecords[wrongRecordsIndex].Values[_columnTypes[i]].Add(new XmlValue(true, valueFromTable));
                }
                wrongRecordsIndex++;
            }
            foreach (var wrongCell in _wrongCells)
            {
                // Kolorowanie komórek
                dataGridViewUnreadFiles.Rows[wrongCell.RowIndex].Cells[wrongCell.ColIndex].Style.BackColor =
                    Color.IndianRed;
                // Zaznaczanie, że zczytana z dGV wartość jest błędna (POPRAWIĆ DLA FAKTUR)
                _wrongRecords[wrongCell.RowIndex].Values[_columnTypes[wrongCell.ColIndex]].First().IsCorrect = false;
            }
        }

        /// <summary>
        ///     Funkcja sprawdza, czy zaznaczony jest checkBox zaznaczający chęć wygenerowania pliku PDF
        /// </summary>
        private void CheckGeneratePDf()
        {
            _generatePdf = checkBoxGeneratePDF.Checked;
        }

        /// <summary>
        ///     Funkcja sprawdza istnienie duplikatów faktur oraz zwraca ich listę (nr faktury - dwa pliki)
        /// </summary>
        /// <param name="files"></param>
        /// <returns></returns>
        private static IEnumerable<KeyValuePair<string, List<DuplicateInvoice>>> CheckForDuplicateInvoices(
            IEnumerable<XmlFile> files)
        {
            var d = new Dictionary<string, List<DuplicateInvoice>>();
            //            foreach (var file in files)
            //            foreach (var invoice in file.InvoiceNumbers)
            //                if (!duplicatesDict.ContainsKey(invoice))
            //                    duplicatesDict.Add(invoice, new List<string> {file.FileName});
            //                else
            //                    duplicatesDict[invoice].Add(file.FileName);
            //            return duplicatesDict.Select(x => x).Where(y => y.Value.Count > 1).ToList();
            foreach (var file in files)
            foreach (var invoice in file.InvoiceNumbers)
                if (!d.ContainsKey(invoice))
                    d.Add(invoice, new List<DuplicateInvoice> {new DuplicateInvoice(file.FileName, file.Mrn)});
                else
                    d[invoice].Add(new DuplicateInvoice(file.FileName, file.Mrn));
            return d.Select(x => x).Where(y => y.Value.Count > 1).ToList();
        }

        /// <summary>
        ///     Funkcja pobiera zawartość komórki z nieprzeczytanymi plikami, na podstawie podanych x i y
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <param name="colIndex"></param>
        /// <returns></returns>
        private string GetUnreadDgvValue(int rowIndex, int colIndex)
        {
            return dataGridViewUnreadFiles.Rows[rowIndex]
                .Cells[colIndex].Value.ToString();
        }

        /// <summary>
        ///     Funkcja wyłuskuje nuery faktur z komórki w tabeli (.Split i tak dalej)
        /// </summary>
        /// <param name="cell"></param>
        /// <returns></returns>
        private static List<string> GetInvoicesFromCell(string cell)
        {
            var invoices = new List<string>();
            var lines = cell.Split(new[] {Environment.NewLine}, StringSplitOptions.None);
            foreach (var line in lines)
            {
                var invoice = line.TrimStart('[').TrimEnd(']');
                invoices.Add(invoice);
            }
            return invoices;
        }

        /// <summary>
        ///     Funkcja wywołuje jedną z funkcji generacji - Unprepared lub Prepared. Dodatowo sprawdza duplikaty faktur i
        ///     wywołuje funkcję czyszczącą po generacji
        /// </summary>
        /// <param name="prepared"></param>
        private void Generate(bool prepared)
        {
            if (prepared) GeneratePrepared();
            if (!prepared) GenerateUnprepared();
            var duplicates = CheckForDuplicateInvoices(_excelFileHandler.PreparedXmlFiles);
            AddDuplicateInvoicesStatistics(duplicates);
            CleanAfterGeneration();
        }

        /// <summary>
        ///     Funkcja generuje plik Excela z danych przygotowanych. Używana podczas naprawiania błędów w plikach XML
        /// </summary>
        private void GeneratePrepared()
        {
            // Jeśli podana ścieżka jest zła
            if (!Directory.Exists(_xmlPath) || _xmlPath == string.Empty)
            {
                MessageBox.Show($@"Podałeś niewłaściwą ścieżkę!");
                return;
            }
            // Zapisanie nazwy umowy do pliku konfiguracyjnego
            File.WriteAllLines(Path.GetFullPath(ConfigFile), new List<string> {_xmlPath, textBoxContractName.Text});
            var pdfCreated = false;
            buttonGenerateExcel.Enabled = false;
            progressBarGenerate.Value = 0;
            progressBarGenerate.Enabled = true;
            var generationResult = _excelFileHandler.WriteExcelFile(_xmlPath, textBoxContractName.Text, true);
            if (!generationResult.FileWriteSuccess)
            {
                buttonGenerateExcel.Enabled = true;
                return;
            }
            progressBarGenerate.Value = 50;
            if (_generatePdf && _excelFileHandler.ReadyToGeneratePdf())
                if (!_excelFileHandler.GeneratePdf())
                {
                    progressBarGenerate.Value = 0;
                }
                else
                {
                    progressBarGenerate.Value = 100;
                    pdfCreated = true;
                }
            else progressBarGenerate.Value = 100;
            buttonGenerateExcel.Enabled = true;
            AddGeneralStatistics(generationResult);
            AddRejectedStatistics(generationResult);
            AddUnreadFilesStatistics(generationResult);
            var fullPdfPath = Path.GetFullPath(_excelFileHandler.GetFileName() + ".pdf");
            if (pdfCreated) Process.Start(fullPdfPath);
        }

        /// <summary>
        ///     Funkcja generuje plik Excela z danych nieprzygotowanych (czyta pliki z dysku).
        ///     Używana tylko podczas odpalenia generacji na 1szej zakładce (nie dla przycisku poprawiania)
        /// </summary>
        private void GenerateUnprepared()
        {
            // Jeśli podana ścieżka jest zła
            if (!Directory.Exists(_xmlPath) || _xmlPath == string.Empty)
            {
                MessageBox.Show($@"Podałeś niewłaściwą ścieżkę!");
                return;
            }
            // Zapisanie nazwy umowy do pliku konfiguracyjnego
            File.WriteAllLines(Path.GetFullPath(ConfigFile), new List<string> {_xmlPath, textBoxContractName.Text});
            var pdfCreated = false;
            buttonGenerateExcel.Enabled = false;
            progressBarGenerate.Value = 0;
            progressBarGenerate.Enabled = true;
            var generationResult = _excelFileHandler.WriteExcelFile(_xmlPath, textBoxContractName.Text, false);
            if (!generationResult.FileWriteSuccess)
            {
                buttonGenerateExcel.Enabled = true;
                return;
            }
            progressBarGenerate.Value = 50;
            CheckGeneratePDf();
            if (_generatePdf && _excelFileHandler.ReadyToGeneratePdf())
                if (!_excelFileHandler.GeneratePdf())
                {
                    progressBarGenerate.Value = 0;
                }
                else
                {
                    progressBarGenerate.Value = 100;
                    pdfCreated = true;
                }
            else progressBarGenerate.Value = 100;
            buttonGenerateExcel.Enabled = true;
            AddGeneralStatistics(generationResult);
            AddRejectedStatistics(generationResult);
            AddUnreadFilesStatistics(generationResult);
            var fullPdfPath = Path.GetFullPath(_excelFileHandler.GetFileName() + ".pdf");
            if (pdfCreated) Process.Start(fullPdfPath);
            buttonFixAll.Enabled = true;
            buttonFixAll.Enabled = true;
        }

        /// <summary>
        ///     Funkcja robi porządek po generacji plików (enable/disable buttonów, odświeżenie booli dla DataBinded eventu)
        /// </summary>
        private void CleanAfterGeneration()
        {
            // Disabling or enabling Fix buttons
            if (dataGridViewUnreadFiles.Rows.Count == 0)
            {
                buttonFixAll.Enabled = false;
                buttonFixOne.Enabled = false;
            }
            else
            {
                buttonFixAll.Enabled = true;
                buttonFixOne.Enabled = true;
            }
            _firstLoad = true;
            _firstTab = true;
        }

        /// <summary>
        ///     Funkcja odpowiada za zupdate'owanie pojedynczego rekordu w pamięci (aby zaraz użyć go, do wpisywania danych do
        ///     Excela)
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <param name="colIndex"></param>
        /// <param name="value"></param>
        private void UpdateRecordsInMemory(int rowIndex, int colIndex, string value)
        {
            switch (colIndex)
            {
                case 0:
                {
                    _wrongRecords[rowIndex].FileName = value;
                    break;
                }
                case 1:
                {
                    _wrongRecords[rowIndex].StringDate = value;
                    break;
                }
                case 2:
                {
                    _wrongRecords[rowIndex].Mrn = value;
                    break;
                }
                case 3:
                {
                    _wrongRecords[rowIndex].Uc = value;
                    break;
                }
                case 4:
                {
                    _wrongRecords[rowIndex].Code = value;
                    break;
                }
                case 5:
                {
                    _wrongRecords[rowIndex].Type = value;
                    break;
                }
            }
        }

        /// <summary>
        ///     Funkcja odpowiada za zupdate'owanie pojedynczego rekordu w pamięci (aby zaraz użyć go, do wpisywania danych do
        ///     Excela).
        ///     Jest to overload, odpowiada on za poprawianie faktur
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <param name="colIndex"></param>
        /// <param name="invoices"></param>
        private void UpdateRecordsInMemory(int rowIndex, int colIndex, IEnumerable<string> invoices)
        {
            if (colIndex != 6) return;
            _wrongRecords[rowIndex].InvoiceNumbers = new List<string>(invoices);
        }

        /// <summary>
        ///     Funkcja aktualizuje _excelFileHandler.preparedXmlFiles wartościami z wrongRecords
        /// </summary>
        private void UpdateRecordsToWrite()
        {
            foreach (var record in _wrongRecords)
            {
                var itemToChange =
                    _excelFileHandler.PreparedXmlFiles.FirstOrDefault(d => d.UniqueId == record.UniqueId);
                if (itemToChange == null) continue;
                itemToChange.UniqueId = record.UniqueId;
                itemToChange.FileName = record.FileName;
                itemToChange.Date = record.Date;
                itemToChange.StringDate = record.StringDate;
                itemToChange.Mrn = record.Mrn;
                itemToChange.Uc = record.Uc;
                itemToChange.Code = record.Code;
                itemToChange.Type = record.Type;
                itemToChange.InvoiceNumbers = new List<string>(record.InvoiceNumbers);
            }
        }
    }
}