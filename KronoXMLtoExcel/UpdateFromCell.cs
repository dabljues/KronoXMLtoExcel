﻿using System;
using System.Collections.Generic;

namespace KronoXMLtoExcel
{
    public class UpdateFromCell
    {
        // Współrzędne komórek (póki co używany jest jedynie rowIndex)
        private readonly CellCoordinates _cellCoordinates;

        // Nowe przeczytane numery faktur, które nadpiszą stare
        public List<string> Invoices;

        // Nowa wartość, która nadpisze starą
        public string NewValue = string.Empty;

        public UpdateFromCell()
        {
        }

        public UpdateFromCell(int x, int y, string newValue)
        {
            _cellCoordinates = new CellCoordinates(x, y);
            NewValue = newValue ?? throw new ArgumentNullException(nameof(newValue));
        }

        public UpdateFromCell(int x, int y, List<string> invoices)
        {
            _cellCoordinates = new CellCoordinates(x, y);
            Invoices = new List<string>(invoices);
        }

        /// <summary>
        ///     Funkcja zwraca indeks wiersza
        /// </summary>
        /// <returns></returns>
        public int RowIndex()
        {
            return _cellCoordinates.RowIndex;
        }

        /// <summary>
        ///     Funkcja zwraca indeks kolumny
        /// </summary>
        /// <returns></returns>
        public int ColIndex()
        {
            return _cellCoordinates.ColIndex;
        }
    }
}