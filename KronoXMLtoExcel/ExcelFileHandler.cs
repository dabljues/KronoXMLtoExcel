﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows;
using DocumentFormat.OpenXml.Spreadsheet;
using Microsoft.Office.Interop.Excel;
using SpreadsheetLight;
using Application = Microsoft.Office.Interop.Excel.Application;
using Color = System.Drawing.Color;

namespace KronoXMLtoExcel
{
    public class ExcelFileHandler
    {
        // Czcionka używana do wygenerowania Excela
        private const string Font = "Calibri";

        // Rozmiar tej czcionki
        private const int FontSizeInfo = 9;

        // Index kolumny, od którego zaczynamy pisanie (w prawo)
        private const int StartColIndex = 1;

        // Index wiersza, od którego zaczynami pisanie (w dół)
        private const int StartRowIndex = 9;

        // Nazwa szablonu, z którego będziemy korzystać do wygenerowania Excela
        private const string TemplateName = "szablon.xlsx";

        // Nazwa pliku, który zostanie wygenerowany
        private string _writtenFilePath = "../new";

        // Słownik plików IE529 do odrzucenia
        private readonly Dictionary<string, string> _applicationsToCancel;

        // Słownik zawierający polskie nazwy miesięcy opdowiadające numerom miesięcy podanym jako klucz
        private readonly Dictionary<int, string> _polishMonths = new Dictionary<int, string>
        {
            {1, "STYCZEŃ"},
            {2, "LUTY"},
            {3, "MARZEC"},
            {4, "KWIECIEŃ"},
            {5, "MAJ"},
            {6, "CZERWIEC"},
            {7, "LIPIEC"},
            {8, "SIERPIEŃ"},
            {9, "WRZESIEŃ"},
            {10, "PAŹDZIERNIK"},
            {11, "LISTOPAD"},
            {12, "GRUDZIEŃ"}
        };

// Bool określający gotowość do generacji PDFa
        private bool _readyToGeneratePdf;

        // Obiekt umożliwiający manipulację plikiem Excela
        private SLDocument _sl;

        // Statystyki
        private Statistics _statistics;

        // Styl obramowania tabeli (na zewnątrz)
        private SLStyle _tableBorderStyle;

        // Styl obramowania tabeli (węwnątrz)
        private SLStyle _tableStyle;

        // Lista plików wcześniej przygotowanych. Podczas pierwszej generacji dodaje się tam wszystkie
        // stworzone XmlFile
        public List<XmlFile> PreparedXmlFiles = new List<XmlFile>();

        public ExcelFileHandler()
        {
            _statistics = new Statistics();
            _applicationsToCancel = new Dictionary<string, string>();
        }

        /// <summary>
        ///     Otworzenie szablonu do edycji (będzie on nietknięty -> Nie Save, a Save As)
        /// </summary>
        /// <returns></returns>
        public bool OpenTemplate()
        {
            try
            {
                _sl = new SLDocument(TemplateName);
                return true;
            }
            catch (Exception)
            {
                MessageBox.Show($"Przed generacją zamknij plik {TemplateName}!");
                return false;
            }
        }

        /// <summary>
        ///     Stworzenie stylów tabeli (nie przypisywanie ich jeszcze do niej)
        /// </summary>
        private void SetTableStyle()
        {
            _tableStyle = _sl.CreateStyle();
            _tableBorderStyle = _sl.CreateStyle();
            _tableStyle.SetLeftBorder(BorderStyleValues.Thin, Color.Black);
            _tableStyle.SetRightBorder(BorderStyleValues.Thin, Color.Black);
            _tableStyle.SetTopBorder(BorderStyleValues.Thin, Color.Black);
            _tableStyle.SetBottomBorder(BorderStyleValues.Thin, Color.Black);
            _tableStyle.Alignment.Horizontal = HorizontalAlignmentValues.Center;
            _tableStyle.Alignment.Vertical = VerticalAlignmentValues.Center;
        }

        /// <summary>
        ///     Czyszczenie zmiennych (listy zgłoszeń do odrzucenia, statystyk) przed kolejną generacją
        /// </summary>
        public void CleanBeforeGeneration(bool fixingAll)
        {
            if (fixingAll)
            {
                _statistics.UnreadFilesInfo.Clear();
                _statistics.UnreadFiles = 0;
            }
            else
            {
                _applicationsToCancel.Clear();
                _statistics = new Statistics();
            }
        }

        /// <summary>
        /// Funkcja odpowiada za zupdate'owanie informacji o naprawionym ręcznie pliku, aby nie pojawiał się już w tabeli
        /// </summary>
        /// <param name="uniqueId"></param>
        public void FixedOne(string uniqueId)
            {
                _statistics.UnreadFiles--;
                _statistics.UnreadFilesInfo.RemoveAll(x =>
                    x[XmlFile.XmlValueType.UniqueId].First().Value == uniqueId);
            }

        /// <summary>
        ///     Getter do pola określającego gotowość do generaci PDFa
        /// </summary>
        /// <returns></returns>
        public bool ReadyToGeneratePdf()
        {
            return _readyToGeneratePdf;
        }

        /// <summary>
        ///     Funkcja upiększa tabelę. Dodaje style (obramowanie, alignment)
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <param name="theSame"></param>
        public void BeautifyTable(int rowIndex, bool theSame)
        {
            // Setting main table style (thin lines everywhere)
            _sl.SetCellStyle(StartRowIndex, StartColIndex, rowIndex - 1, StartColIndex + 6, _tableStyle);
            if (theSame) return;
            // Setting the style of the borders of the table
            // Medium line under the table
            _tableBorderStyle.SetBottomBorder(BorderStyleValues.Medium, Color.Black);
            _sl.SetCellStyle(rowIndex - 1, StartColIndex, rowIndex - 1, StartColIndex + 6, _tableBorderStyle);
            // Medium line on the right hand side of the table
            _tableBorderStyle.RemoveBorder();
            _tableBorderStyle.SetRightBorder(BorderStyleValues.Medium, Color.Black);
            _sl.SetCellStyle(StartRowIndex, StartColIndex + 6, rowIndex - 2, StartColIndex + 6, _tableBorderStyle);
            // Medium line on the left hand side of the table
            _tableBorderStyle.RemoveBorder();
            _tableBorderStyle.SetLeftBorder(BorderStyleValues.Medium, Color.Black);
            _sl.SetCellStyle(StartRowIndex, StartColIndex, rowIndex - 2, StartColIndex, _tableBorderStyle);
            // The right bottom corner of the table
            _tableBorderStyle.RemoveBorder();
            _tableBorderStyle.SetRightBorder(BorderStyleValues.Medium, Color.Black);
            _tableBorderStyle.SetBottomBorder(BorderStyleValues.Medium, Color.Black);
            _sl.SetCellStyle(rowIndex - 1, StartColIndex + 6, _tableBorderStyle);
            // The left bottom corner of the table
            _tableBorderStyle.RemoveBorder();
            _tableBorderStyle.SetLeftBorder(BorderStyleValues.Medium, Color.Black);
            _tableBorderStyle.SetBottomBorder(BorderStyleValues.Medium, Color.Black);
            _sl.SetCellStyle(rowIndex - 1, StartColIndex, _tableBorderStyle);
        }

        /// <summary>
        ///     Funckja dodaje info na górze pliku (nazwa umowy, miesiąc, ilośc zgłoszeń, cena)
        /// </summary>
        /// <param name="lp"></param>
        /// <param name="validFiles"></param>
        /// <param name="contractName"></param>
        public void AddInfo(int lp, int validFiles, string contractName)
        {
            var style = _sl.CreateStyle();
            style.SetFont(Font, FontSizeInfo);
            style.Font.Bold = true;
            var firstCell =
                $"{contractName}";
            var year = DateTime.Today.AddMonths(-1).Year;
            var month = DateTime.Today.AddMonths(-1).Month;
             SetFileName(year, month);
            var polishMonth = _polishMonths[month];
            var secondCell =
                $"Specyfikacja do faktury za {polishMonth} {year} - {validFiles} zgłoszeń celnych eksportowych x 75,00 PLN  = {validFiles * 75.0:F2} PLN";
            _sl.SetCellValue(4, 1, firstCell);
            _sl.SetCellValue(5, 1, secondCell);
            _sl.SetCellStyle(4, 1, style);
            _sl.SetCellStyle(5, 1, style);
        }

        /// <summary>
        /// Funkcja ustawiana nazwę pliku wyjściowego za pomocą roku i miesiąca
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        public void SetFileName(int year, int month)
        {
            var stringMonth = month < 10 ? $"0{month}" : $"{month}";
            _writtenFilePath = $"../{year}.{stringMonth}_specyfikacja do faktury";
        }

        /// <summary>
        /// Funkcja zwraca nazwę pliku wyjściowego (bez rozszerzenia)
        /// </summary>
        /// <returns></returns>
        public string GetFileName()
        {
            return _writtenFilePath;
        }

        /// <summary>
        ///     Funkcja zapisuje wygenerowany plik Excela
        /// </summary>
        /// <returns></returns>
        public bool SaveExcelFile()
        {
            var excelName = _writtenFilePath + ".xlsx";
            try
            {
                _sl.SaveAs(Path.GetFullPath(excelName));
            }
            catch (IOException)
            {
                MessageBox.Show($"Przed generacją pliku Excela zamnkij plik {Path.GetFullPath(excelName)}!");
                return false;
            }
            if (!OpenTemplate()) return false;
            _readyToGeneratePdf = true;
            return true;
        }

        /// <summary>
        ///     Funkcja generuje PDF
        /// </summary>
        /// <returns></returns>
        public bool GeneratePdf()
        {
            var pdfName = _writtenFilePath + ".pdf";
            var fullExcelPath = Path.GetFullPath(_writtenFilePath + ".xlsx");
            var fullPdfPath = Path.GetFullPath(pdfName);
            var success = false;
            if (!_readyToGeneratePdf)
            {
                MessageBox.Show("Nie udało Ci się wcześniej utworzyć pliku!");
                return false;
            }
            //            var converter = new SautinSoft.ExcelToPdf();
            //            converter.ConvertFile(_writtenFilePath, _pdfWritePath);
            var app = new Application
            {
                Visible = false,
                DisplayAlerts = false
            };
            var wkb = app.Workbooks.Open(fullExcelPath, ReadOnly: true);
            try
            {
                wkb.ExportAsFixedFormat(XlFixedFormatType.xlTypePDF, fullPdfPath);
                success = true;
            }
            catch (COMException)
            {
                MessageBox.Show($"Przed generacją pliku PDF zamknij plik {fullPdfPath}!");
            }
            wkb.Close();
            app.Quit();
            return success;
        }

        /// <summary>
        ///     Funkcja wpisuje dane do Excela
        /// </summary>
        /// <param name="xmlPath"></param>
        /// <param name="contractName"></param>
        /// <param name="prepared"></param>
        /// <returns></returns>
        public Statistics WriteExcelFile(string xmlPath, string contractName, bool prepared)
        {
            if (prepared) return WriteFromPrepared(contractName);
            return WriteFromUnprepared(xmlPath, contractName);
        }

        /// <summary>
        ///     Funkcja generuje plik Excela z nieprzygotowanych plików (zazwyczaj pierwsze użycie)
        /// </summary>
        /// <param name="xmlPath"></param>
        /// <param name="contractName"></param>
        /// <returns></returns>
        private Statistics WriteFromUnprepared(string xmlPath, string contractName)
        {
            PreparedXmlFiles.Clear();
            if (!OpenTemplate())
            {
                _statistics.FileWriteSuccess = false;
                return _statistics;
            }
            CleanBeforeGeneration(false);
            SetTableStyle();
            // Liczba porządkowa w tabeli
            var lp = 1;
            var rowIndex = StartRowIndex;
            // true -> cała tabela ma jednakowe kreski, fasle -> obramowanie jest grubsze
            // Lista plików XML IE529
            var files529 = Directory.GetFiles(xmlPath);
            var lastPageBreak = 0;
            var validFiles = 0;
            // Lista plików XML IE514
            var files514 = Directory.GetFiles(xmlPath + '\\' + "IE514");
            // Zczytywanie MRNów do odrzucenia z listy plików IE529
            foreach (var file in files514)
            {
                var xmlFile = new XmlFile(file, "-1");
                if (!xmlFile.CheckNip()) continue;
                if (!_applicationsToCancel.ContainsKey(xmlFile.Read514File()))
                    _applicationsToCancel.Add(xmlFile.Read514File(), string.Empty);
            }
            var id = 0;
            foreach (var file in files529)
            {
                var xmlFile = new XmlFile(file, id.ToString());
                if (!xmlFile.CheckNip())
                {
                    _statistics.BadNipFiles++;
                    id++;
                    continue;
                }
                if (!xmlFile.Read529File())
                {
                    xmlFile.IsCorrectOrFixed = false;
                    _statistics.UnreadFilesInfo.Add(xmlFile.Values);
                    PreparedXmlFiles.Add(xmlFile);
                    _statistics.UnreadFiles++;
                    id++;
                    continue;
                }
                if (_applicationsToCancel.ContainsKey(xmlFile.Mrn))
                {
                    _applicationsToCancel[xmlFile.Mrn] = xmlFile.FileName;
                    _statistics.RejectedBy514Files++;
                    id++;
                    continue;
                }
                xmlFile.IsCorrectOrFixed = true;
                PreparedXmlFiles.Add(xmlFile);
                // Page Break
                if (rowIndex + xmlFile.InvoiceNumbers.Count > lastPageBreak + 47)
                {
                    _sl.InsertPageBreak(rowIndex, -1);
                    lastPageBreak = rowIndex;
                }
                // Lp.
                _sl.SetCellValue(rowIndex, 1, lp);
                // Data
                _sl.SetCellValue(rowIndex, 2, xmlFile.Date);
                // MRN
                _sl.SetCellValue(rowIndex, 3, xmlFile.Mrn);
                // UC Wywozu
                _sl.SetCellValue(rowIndex, 4, xmlFile.Uc);
                // Kod
                _sl.SetCellValue(rowIndex, 5, xmlFile.Code);
                // Towar
                _sl.SetCellValue(rowIndex, 6, "Papier");
                // Faktura
                for (var colIndex = StartColIndex; colIndex < StartColIndex + 6; colIndex++)
                    _sl.MergeWorksheetCells(rowIndex, colIndex, rowIndex + xmlFile.InvoiceNumbers.Count - 1, colIndex);
                foreach (var invoiceNumber in xmlFile.InvoiceNumbers)
                {
                    _sl.SetCellValue(rowIndex, StartColIndex + 6, invoiceNumber);
                    rowIndex++;
                }
                id++;
                lp++;
                validFiles++;
            }
            // Czysczenie słownika z IE514, które nie anulowały żadnego z IE529
            var notRejected = new List<string>();
            foreach (var file in _applicationsToCancel)
                if (file.Value == string.Empty)
                    notRejected.Add(file.Key);
            foreach (var key in notRejected)
                _applicationsToCancel.Remove(key);
            BeautifyTable(rowIndex, false);
            var ps = _sl.GetPageSettings();
            ps.View = SheetViewValues.PageBreakPreview;
            _sl.SetPageSettings(ps);
            AddInfo(lp, validFiles, contractName);
            if (!SaveExcelFile())
            {
                _statistics.FileWriteSuccess = false;
                return _statistics;
            }
            _statistics.FileWriteSuccess = true;
            _statistics.RejectedFilesInfo = _applicationsToCancel;
            return _statistics;
        }

        /// <summary>
        ///     Funkcja generuje plik Excela z przygotowanych wcześniej plików (przy naprawianiu błędów)
        /// </summary>
        /// <param name="contractName"></param>
        /// <returns></returns>
        private Statistics WriteFromPrepared(string contractName)
        {
            if (!OpenTemplate())
            {
                _statistics.FileWriteSuccess = false;
                return _statistics;
            }
//            CleanBeforeGeneration(true);
            SetTableStyle();
            // Liczba porządkowa w tabeli
            var lp = 1;
            var rowIndex = StartRowIndex;
            // true -> cała tabela ma jednakowe kreski, fasle -> obramowanie jest grubsze
            const bool theSame = false;
            var lastPageBreak = 0;
            var validFiles = 0;
            foreach (var xmlFile in PreparedXmlFiles)
            {
                if (!xmlFile.IsCorrectOrFixed) continue;
                // Page Break
                if (rowIndex + xmlFile.InvoiceNumbers.Count > lastPageBreak + 47)
                {
                    _sl.InsertPageBreak(rowIndex, -1);
                    lastPageBreak = rowIndex;
                }
                // Lp.
                _sl.SetCellValue(rowIndex, 1, lp);
                // Data
                _sl.SetCellValue(rowIndex, 2, DateTime.Parse(xmlFile.StringDate).Date);
                // MRN
                _sl.SetCellValue(rowIndex, 3, xmlFile.Mrn);
                // UC Wywozu
                _sl.SetCellValue(rowIndex, 4, xmlFile.Uc);
                // Kod
                _sl.SetCellValue(rowIndex, 5, xmlFile.Code);
                // Towar
                _sl.SetCellValue(rowIndex, 6, "Papier");
                // Faktura
                for (var colIndex = StartColIndex; colIndex < StartColIndex + 6; colIndex++)
                    _sl.MergeWorksheetCells(rowIndex, colIndex, rowIndex + xmlFile.InvoiceNumbers.Count - 1, colIndex);
                foreach (var invoiceNumber in xmlFile.InvoiceNumbers)
                {
                    _sl.SetCellValue(rowIndex, StartColIndex + 6, invoiceNumber);
                    rowIndex++;
                }
                lp++;
                validFiles++;
            }
            // Czysczenie słownika z IE514, które nie anulowały żadnego z IE529
            var notRejected = new List<string>();
            foreach (var file in _applicationsToCancel)
                if (file.Value == string.Empty)
                    notRejected.Add(file.Key);
            foreach (var key in notRejected)
                _applicationsToCancel.Remove(key);
            BeautifyTable(rowIndex, theSame);
            var ps = _sl.GetPageSettings();
            ps.View = SheetViewValues.PageBreakPreview;
            _sl.SetPageSettings(ps);
            AddInfo(lp, validFiles, contractName);
            if (!SaveExcelFile())
            {
                _statistics.FileWriteSuccess = false;
                return _statistics;
            }
            _statistics.FileWriteSuccess = true;
            _statistics.RejectedFilesInfo = _applicationsToCancel;
            return _statistics;
        }
    }
}