﻿using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace KronoXMLtoExcel
{
    public class ConfigReader
    {
        /// <summary>
        ///     Funkcja wczytuje poprzednio wybraną ścieżkę do plików XML
        /// </summary>
        /// <param name="configFile"></param>
        /// <returns></returns>
        public string LoadPrevious(string configFile)
        {
            if (!File.Exists(configFile)) return string.Empty;
            try
            {
                var path = File.ReadAllLines(configFile).First();
                return path;
            }
            catch (InvalidOperationException)
            {
                return string.Empty;
            }
        }

        /// <summary>
        ///     Funkcja rozdziela ścieżkę do plików XML (usuwając Folder Selection)
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private static string SplitPath(string path)
        {
            const char separator = '\\';
            var chunks = path.Split(separator);
            var xmlPath = chunks[0];
            for (var i = 1; i < chunks.Length - 1; i++)
                xmlPath += separator + chunks[i];
            return xmlPath;
        }

        /// <summary>
        ///     Funkcja zwraca ostatnio używaną nazwę umowy
        /// </summary>
        /// <param name="configFile"></param>
        /// <returns></returns>
        public string GetContractName(string configFile)
        {
            if (!File.Exists(configFile)) return string.Empty;
            try
            {
                var contractName = File.ReadAllLines(configFile).Skip(1).First();
                return contractName;
            }
            catch (InvalidOperationException)
            {
                return string.Empty;
            }
        }

        /// <summary>
        ///     Funkcja zwraca ostatnio używaną ścieżkę do plików XML
        /// </summary>
        /// <param name="configFile"></param>
        /// <returns></returns>
        public string GetXmlPath(string configFile)
        {
            var checkPrev = LoadPrevious(configFile);
            var xmlDir = new OpenFileDialog
            {
                InitialDirectory = checkPrev != string.Empty ? checkPrev : Directory.GetCurrentDirectory(),
                ValidateNames = false,
                CheckFileExists = false,
                CheckPathExists = true,
                FileName = "Folder Selection."
            };
            return xmlDir.ShowDialog() == DialogResult.OK ? SplitPath(xmlDir.FileName) : "";
        }
    }
}