﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace KronoXMLtoExcel
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxXMLPath = new System.Windows.Forms.TextBox();
            this.buttonSelectXMLPath = new System.Windows.Forms.Button();
            this.labelXMLPath = new System.Windows.Forms.Label();
            this.buttonGenerateExcel = new System.Windows.Forms.Button();
            this.progressBarGenerate = new System.Windows.Forms.ProgressBar();
            this.checkBoxGeneratePDF = new System.Windows.Forms.CheckBox();
            this.tabControlMain = new System.Windows.Forms.TabControl();
            this.tabPageGeneration = new System.Windows.Forms.TabPage();
            this.labelContractName = new System.Windows.Forms.Label();
            this.textBoxContractName = new System.Windows.Forms.TextBox();
            this.tabPageStatistics = new System.Windows.Forms.TabPage();
            this.dataGridViewDuplicateInvoices = new System.Windows.Forms.DataGridView();
            this.labelDuplicateInvoices = new System.Windows.Forms.Label();
            this.buttonFixOne = new System.Windows.Forms.Button();
            this.buttonFixAll = new System.Windows.Forms.Button();
            this.labelUnreadFiles = new System.Windows.Forms.Label();
            this.labelRejectedBy514 = new System.Windows.Forms.Label();
            this.labelGeneralStatistics = new System.Windows.Forms.Label();
            this.dataGridViewUnreadFiles = new System.Windows.Forms.DataGridView();
            this.dataGridViewRejectedBy514 = new System.Windows.Forms.DataGridView();
            this.dataGridViewStatistics = new System.Windows.Forms.DataGridView();
            this.tabControlMain.SuspendLayout();
            this.tabPageGeneration.SuspendLayout();
            this.tabPageStatistics.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDuplicateInvoices)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewUnreadFiles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewRejectedBy514)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStatistics)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxXMLPath
            // 
            this.textBoxXMLPath.Location = new System.Drawing.Point(259, 88);
            this.textBoxXMLPath.Name = "textBoxXMLPath";
            this.textBoxXMLPath.Size = new System.Drawing.Size(185, 20);
            this.textBoxXMLPath.TabIndex = 0;
            // 
            // buttonSelectXMLPath
            // 
            this.buttonSelectXMLPath.Location = new System.Drawing.Point(450, 86);
            this.buttonSelectXMLPath.Name = "buttonSelectXMLPath";
            this.buttonSelectXMLPath.Size = new System.Drawing.Size(24, 23);
            this.buttonSelectXMLPath.TabIndex = 1;
            this.buttonSelectXMLPath.Text = "...";
            this.buttonSelectXMLPath.UseVisualStyleBackColor = true;
            this.buttonSelectXMLPath.Click += new System.EventHandler(this.buttonSelectXMLPath_Click);
            // 
            // labelXMLPath
            // 
            this.labelXMLPath.AutoSize = true;
            this.labelXMLPath.Location = new System.Drawing.Point(105, 91);
            this.labelXMLPath.Name = "labelXMLPath";
            this.labelXMLPath.Size = new System.Drawing.Size(148, 13);
            this.labelXMLPath.TabIndex = 2;
            this.labelXMLPath.Text = "Wybierz folder z plikami XML: ";
            // 
            // buttonGenerateExcel
            // 
            this.buttonGenerateExcel.Location = new System.Drawing.Point(301, 172);
            this.buttonGenerateExcel.Name = "buttonGenerateExcel";
            this.buttonGenerateExcel.Size = new System.Drawing.Size(75, 23);
            this.buttonGenerateExcel.TabIndex = 3;
            this.buttonGenerateExcel.Text = "Generuj";
            this.buttonGenerateExcel.UseVisualStyleBackColor = true;
            this.buttonGenerateExcel.Click += new System.EventHandler(this.buttonGenerateExcel_Click);
            // 
            // progressBarGenerate
            // 
            this.progressBarGenerate.Location = new System.Drawing.Point(340, 212);
            this.progressBarGenerate.Name = "progressBarGenerate";
            this.progressBarGenerate.Size = new System.Drawing.Size(200, 43);
            this.progressBarGenerate.TabIndex = 4;
            // 
            // checkBoxGeneratePDF
            // 
            this.checkBoxGeneratePDF.AutoSize = true;
            this.checkBoxGeneratePDF.Location = new System.Drawing.Point(165, 176);
            this.checkBoxGeneratePDF.Name = "checkBoxGeneratePDF";
            this.checkBoxGeneratePDF.Size = new System.Drawing.Size(105, 17);
            this.checkBoxGeneratePDF.TabIndex = 5;
            this.checkBoxGeneratePDF.Text = "Generacja PDFa";
            this.checkBoxGeneratePDF.UseVisualStyleBackColor = true;
            // 
            // tabControlMain
            // 
            this.tabControlMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControlMain.Controls.Add(this.tabPageGeneration);
            this.tabControlMain.Controls.Add(this.tabPageStatistics);
            this.tabControlMain.Location = new System.Drawing.Point(0, 0);
            this.tabControlMain.Name = "tabControlMain";
            this.tabControlMain.SelectedIndex = 0;
            this.tabControlMain.Size = new System.Drawing.Size(1027, 638);
            this.tabControlMain.TabIndex = 6;
            // 
            // tabPageGeneration
            // 
            this.tabPageGeneration.Controls.Add(this.labelContractName);
            this.tabPageGeneration.Controls.Add(this.textBoxContractName);
            this.tabPageGeneration.Controls.Add(this.progressBarGenerate);
            this.tabPageGeneration.Controls.Add(this.checkBoxGeneratePDF);
            this.tabPageGeneration.Controls.Add(this.textBoxXMLPath);
            this.tabPageGeneration.Controls.Add(this.buttonSelectXMLPath);
            this.tabPageGeneration.Controls.Add(this.buttonGenerateExcel);
            this.tabPageGeneration.Controls.Add(this.labelXMLPath);
            this.tabPageGeneration.Location = new System.Drawing.Point(4, 22);
            this.tabPageGeneration.Name = "tabPageGeneration";
            this.tabPageGeneration.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageGeneration.Size = new System.Drawing.Size(1019, 612);
            this.tabPageGeneration.TabIndex = 0;
            this.tabPageGeneration.Text = "Generacja";
            this.tabPageGeneration.UseVisualStyleBackColor = true;
            // 
            // labelContractName
            // 
            this.labelContractName.AutoSize = true;
            this.labelContractName.Location = new System.Drawing.Point(171, 128);
            this.labelContractName.Name = "labelContractName";
            this.labelContractName.Size = new System.Drawing.Size(82, 13);
            this.labelContractName.TabIndex = 7;
            this.labelContractName.Text = "Nazwa umowy: ";
            // 
            // textBoxContractName
            // 
            this.textBoxContractName.Location = new System.Drawing.Point(259, 125);
            this.textBoxContractName.Name = "textBoxContractName";
            this.textBoxContractName.Size = new System.Drawing.Size(418, 20);
            this.textBoxContractName.TabIndex = 6;
            // 
            // tabPageStatistics
            // 
            this.tabPageStatistics.Controls.Add(this.dataGridViewDuplicateInvoices);
            this.tabPageStatistics.Controls.Add(this.labelDuplicateInvoices);
            this.tabPageStatistics.Controls.Add(this.buttonFixOne);
            this.tabPageStatistics.Controls.Add(this.buttonFixAll);
            this.tabPageStatistics.Controls.Add(this.labelUnreadFiles);
            this.tabPageStatistics.Controls.Add(this.labelRejectedBy514);
            this.tabPageStatistics.Controls.Add(this.labelGeneralStatistics);
            this.tabPageStatistics.Controls.Add(this.dataGridViewUnreadFiles);
            this.tabPageStatistics.Controls.Add(this.dataGridViewRejectedBy514);
            this.tabPageStatistics.Controls.Add(this.dataGridViewStatistics);
            this.tabPageStatistics.Location = new System.Drawing.Point(4, 22);
            this.tabPageStatistics.Name = "tabPageStatistics";
            this.tabPageStatistics.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageStatistics.Size = new System.Drawing.Size(1019, 612);
            this.tabPageStatistics.TabIndex = 1;
            this.tabPageStatistics.Text = "Statystyki";
            this.tabPageStatistics.UseVisualStyleBackColor = true;
            // 
            // dataGridViewDuplicateInvoices
            // 
            this.dataGridViewDuplicateInvoices.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewDuplicateInvoices.Location = new System.Drawing.Point(360, 368);
            this.dataGridViewDuplicateInvoices.Name = "dataGridViewDuplicateInvoices";
            this.dataGridViewDuplicateInvoices.Size = new System.Drawing.Size(643, 236);
            this.dataGridViewDuplicateInvoices.TabIndex = 12;
            // 
            // labelDuplicateInvoices
            // 
            this.labelDuplicateInvoices.AutoSize = true;
            this.labelDuplicateInvoices.Location = new System.Drawing.Point(363, 352);
            this.labelDuplicateInvoices.Name = "labelDuplicateInvoices";
            this.labelDuplicateInvoices.Size = new System.Drawing.Size(84, 13);
            this.labelDuplicateInvoices.TabIndex = 11;
            this.labelDuplicateInvoices.Text = "Duplikaty faktur:";
            // 
            // buttonFixOne
            // 
            this.buttonFixOne.Enabled = false;
            this.buttonFixOne.Location = new System.Drawing.Point(877, 304);
            this.buttonFixOne.Name = "buttonFixOne";
            this.buttonFixOne.Size = new System.Drawing.Size(126, 45);
            this.buttonFixOne.TabIndex = 10;
            this.buttonFixOne.Text = "Popraw wybrany";
            this.buttonFixOne.UseVisualStyleBackColor = true;
            this.buttonFixOne.Click += new System.EventHandler(this.buttonFixOne_Click);
            // 
            // buttonFixAll
            // 
            this.buttonFixAll.Enabled = false;
            this.buttonFixAll.Location = new System.Drawing.Point(749, 304);
            this.buttonFixAll.Name = "buttonFixAll";
            this.buttonFixAll.Size = new System.Drawing.Size(126, 45);
            this.buttonFixAll.TabIndex = 9;
            this.buttonFixAll.Text = "Popraw wszystkie";
            this.buttonFixAll.UseVisualStyleBackColor = true;
            this.buttonFixAll.Click += new System.EventHandler(this.buttonFixAll_Click);
            // 
            // labelUnreadFiles
            // 
            this.labelUnreadFiles.AutoSize = true;
            this.labelUnreadFiles.Location = new System.Drawing.Point(661, 32);
            this.labelUnreadFiles.Name = "labelUnreadFiles";
            this.labelUnreadFiles.Size = new System.Drawing.Size(75, 13);
            this.labelUnreadFiles.TabIndex = 8;
            this.labelUnreadFiles.Text = "Pliki z błędami";
            // 
            // labelRejectedBy514
            // 
            this.labelRejectedBy514.AutoSize = true;
            this.labelRejectedBy514.Location = new System.Drawing.Point(104, 177);
            this.labelRejectedBy514.Name = "labelRejectedBy514";
            this.labelRejectedBy514.Size = new System.Drawing.Size(163, 13);
            this.labelRejectedBy514.TabIndex = 7;
            this.labelRejectedBy514.Text = "Pliki XML odrzucone przez IE514";
            // 
            // labelGeneralStatistics
            // 
            this.labelGeneralStatistics.AutoSize = true;
            this.labelGeneralStatistics.Location = new System.Drawing.Point(143, 32);
            this.labelGeneralStatistics.Name = "labelGeneralStatistics";
            this.labelGeneralStatistics.Size = new System.Drawing.Size(87, 13);
            this.labelGeneralStatistics.TabIndex = 6;
            this.labelGeneralStatistics.Text = "Statystyki ogólne";
            // 
            // dataGridViewUnreadFiles
            // 
            this.dataGridViewUnreadFiles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewUnreadFiles.Location = new System.Drawing.Point(366, 48);
            this.dataGridViewUnreadFiles.Name = "dataGridViewUnreadFiles";
            this.dataGridViewUnreadFiles.Size = new System.Drawing.Size(637, 250);
            this.dataGridViewUnreadFiles.TabIndex = 5;
            this.dataGridViewUnreadFiles.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dataGridViewUnreadFiles_DataBindingComplete);
            // 
            // dataGridViewRejectedBy514
            // 
            this.dataGridViewRejectedBy514.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewRejectedBy514.Location = new System.Drawing.Point(20, 193);
            this.dataGridViewRejectedBy514.Name = "dataGridViewRejectedBy514";
            this.dataGridViewRejectedBy514.Size = new System.Drawing.Size(326, 412);
            this.dataGridViewRejectedBy514.TabIndex = 4;
            // 
            // dataGridViewStatistics
            // 
            this.dataGridViewStatistics.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewStatistics.Location = new System.Drawing.Point(21, 48);
            this.dataGridViewStatistics.Name = "dataGridViewStatistics";
            this.dataGridViewStatistics.Size = new System.Drawing.Size(325, 105);
            this.dataGridViewStatistics.TabIndex = 3;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1025, 639);
            this.Controls.Add(this.tabControlMain);
            this.Name = "FormMain";
            this.Text = "XML -> Excel";
            this.tabControlMain.ResumeLayout(false);
            this.tabPageGeneration.ResumeLayout(false);
            this.tabPageGeneration.PerformLayout();
            this.tabPageStatistics.ResumeLayout(false);
            this.tabPageStatistics.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDuplicateInvoices)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewUnreadFiles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewRejectedBy514)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStatistics)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TextBox textBoxXMLPath;
        private Button buttonSelectXMLPath;
        private Label labelXMLPath;
        private Button buttonGenerateExcel;
        private ProgressBar progressBarGenerate;
        private CheckBox checkBoxGeneratePDF;
        private TabControl tabControlMain;
        private TabPage tabPageGeneration;
        private TabPage tabPageStatistics;
        private DataGridView dataGridViewStatistics;
        private DataGridView dataGridViewRejectedBy514;
        private Label labelContractName;
        private TextBox textBoxContractName;
        private DataGridView dataGridViewUnreadFiles;
        private Label labelGeneralStatistics;
        private Label labelUnreadFiles;
        private Label labelRejectedBy514;
        private Button buttonFixOne;
        private Button buttonFixAll;
        private DataGridView dataGridViewDuplicateInvoices;
        private Label labelDuplicateInvoices;
    }
}

