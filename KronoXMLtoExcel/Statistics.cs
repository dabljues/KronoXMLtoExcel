﻿using System.Collections.Generic;

namespace KronoXMLtoExcel
{
    public class Statistics
    {
        // Słownik przechowujący informację nt. plików odrzuconych przez pliki IE514.
        // Zawiera nazwę odrzuconego pliku IE529 oraz jego MRN (który był zgodny z MRNem w którymś pliku IE514)
        public Dictionary<string, string> RejectedFilesInfo;

        // Lista słowników, z których każdy zawiera informację na temat każdego potrzebnego nam atrybutu XML.
        // Informacja ta to lista, która zawsze ma 1 element, oprócz przypadku z fakturami -> tam może mieć więcej
        public List<Dictionary<XmlFile.XmlValueType, List<XmlValue>>> UnreadFilesInfo;

        public Statistics()
        {
            UnreadFilesInfo = new List<Dictionary<XmlFile.XmlValueType, List<XmlValue>>>();
        }

        // Informacja nt. tego, czy udało nam się zapisać plik
        public bool FileWriteSuccess { get; set; }

        // Licznik plików IE529 ze złym NIPem
        public int BadNipFiles { get; set; }

        // Licznik plików IE529 odrzuconych przez pliki IE514
        public int RejectedBy514Files { get; set; }

        // Licznik plików, których nie udało się przeczytać/z błędami
        public int UnreadFiles { get; set; }
    }
}