﻿using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace KronoXMLtoExcel
{
    public class XmlPathGetter
    {
        private const string PrevDirFile = "prev.txt";

        public string LoadPrevious()
        {
            return File.Exists(PrevDirFile) ? File.ReadAllLines(PrevDirFile).First() : string.Empty;
        }

        private static string SplitPath(string path)
        {
            const char separator = '\\';
            var chunks = path.Split(separator);
            var xmlPath = chunks[0];
            for (var i = 1; i < chunks.Length - 1; i++)
                xmlPath += separator + chunks[i];
            return xmlPath;
        }

        public string GetXmlPath()
        {
            var checkPrev = LoadPrevious();
            var xmlDir = new OpenFileDialog
            {
                InitialDirectory = checkPrev != string.Empty ? checkPrev : Directory.GetCurrentDirectory(),
                ValidateNames = false,
                CheckFileExists = false,
                CheckPathExists = true,
                FileName = "Folder Selection."
            };
            return xmlDir.ShowDialog() == DialogResult.OK ? SplitPath(xmlDir.FileName) : "";
        }
    }
}