﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace KronoXMLtoExcel
{
    public class XmlFile
    {
        // Enum określający czym jest podany string (wszystkie wartości wczytane z XML będą stringami)
        public enum XmlValueType
        {
            UniqueId,
            Name,
            Date,
            Mrn,
            Uc,
            Code,
            Desc,
            Invoice
        }

        // Obiekt obsługujący plik XML
        private readonly XDocument _doc;

        // NIP, który nas interesuje. Każdy plik z innym NIPem będzie odrzucany
        private const string Nip = "PL6731657551";

        // Atrybuty pliku XML, które będa się znajdowały w wygenerowanym pliku
        public string FileName;

        public DateTime Date;
        public string Mrn;
        public string Uc;
        public string Code;
        public string Type;
        public List<string> InvoiceNumbers;

        // Misc
        public string UniqueId;

        public string StringDate;

        public int RowIndexInTable;

        public bool IsCorrectOrFixed;

        // Lista wartości pliku XML. Będzie wykorzystywana tylko przy wypełnianiu tabeli z plikami z błędami
        public Dictionary<XmlValueType, List<XmlValue>> Values;

        // Słownik mapujący nazwy atrybutów z plików XML do nazw atrybutów w kodzie
        public Dictionary<string, string> XmlAttributesNames;

        public XmlFile(string id)
        {
            UniqueId = id;
            InvoiceNumbers = new List<string>();
            InitializeDictionaries();
        }

        public XmlFile(string path, string id)
        {
            // Otworzenie pliku XML
            _doc = XDocument.Load(path);
            // Wyłuskanie nazwy pliku z pełnej ścieżki do pliku
            FileName = path.Split('\\').Last();
            UniqueId = id;
            InvoiceNumbers = new List<string>();
            InitializeDictionaries();
        }

        /// <summary>
        ///     Funkcja inicjalizuje słowniki wykorzystywane dalej w programie
        /// </summary>
        private void InitializeDictionaries()
        {
            XmlAttributesNames = new Dictionary<string, string>
            {
                {"DataPrzyjecia", "Data"},
                {"MRN", "MRN"},
                {"UCWywozu", "UCWywozu"},
                {"KodTowarowy", "Kod"},
                {"OpisTowaru", "Towar"},
                {"DokumentWymagany", "Faktura"}
            };
            Values = new Dictionary<XmlValueType, List<XmlValue>>
            {
                {XmlValueType.UniqueId, new List<XmlValue>()},
                {XmlValueType.Name, new List<XmlValue>()},
                {XmlValueType.Date, new List<XmlValue>()},
                {XmlValueType.Mrn, new List<XmlValue>()},
                {XmlValueType.Uc, new List<XmlValue>()},
                {XmlValueType.Code, new List<XmlValue>()},
                {XmlValueType.Desc, new List<XmlValue>()},
                {XmlValueType.Invoice, new List<XmlValue>()}
            };
        }

        /// <summary>
        ///     Funkcja sprawdza, czy danym pliku NIP jest zgodny z NIPem, którego oczekujemy
        /// </summary>
        /// <param name="tag"></param>
        /// <returns></returns>
        public bool ValidNip(string tag)
        {
            var c = _doc.Descendants().Select(x => x).FirstOrDefault(y => y.Name.LocalName == tag);
            return c?.Attribute("TIN")?.Value == Nip;
        }

        /// <summary>
        ///     Funkcja zwraca tag XML, czyli <costam></costam>, w którym będziemy czytać atrybuty
        /// </summary>
        /// <param name="tag"></param>
        /// <returns></returns>
        public XElement GetTag(string tag)
        {
            return _doc.Descendants().Select(x => x).FirstOrDefault(y => y.Name.LocalName == tag);
        }

        /// <summary>
        ///     Funkcja zwraca iterabla zawierającego numery faktów z pliku
        /// </summary>
        /// <param name="tag"></param>
        /// <returns></returns>
        public IEnumerable<XElement> GetInvoices(string tag)
        {
            return _doc.Descendants().Select(x => x)
                .Where(y => y.Name.LocalName == tag &&
                            (y.Attribute("Kod")?.Value == "N380" || y.Attribute("Kod")?.Value == "N325"));
        }

        /// <summary>
        ///     Funkcja wywołuje funkcję sprawdzającą NIP, podając jej tag XML, w którym ten NIP się znajduje
        /// </summary>
        /// <returns></returns>
        public bool CheckNip()
        {
            // Filtrowanie po NIPie - jeśli zły, odrzucamy całe zamówienie (w metodzie, która wywowłała Read529File())
            if (!ValidNip("ZglaszajacyPrzedstawiciel")) return false;
            return true;
        }

        /// <summary>
        ///     Funckja sprawdza poprawność podanego atrybutu XML
        /// </summary>
        /// <param name="xmlValue"></param>
        /// <param name="xmlValueType"></param>
        /// <returns></returns>
        private bool CheckXmlValue(string xmlValue, XmlValueType xmlValueType)
        {
            // Sprawdzenie, czy atrybut nie jest pusty (nie powinien)
            if (string.IsNullOrWhiteSpace(xmlValue))
                return false;
            switch (xmlValueType)
            {
                case XmlValueType.Date:
                {
                    // Sprawdzenie, czy można przekonwertować datę (jeśli nie -> data jest zła)
                    if (!DateTime.TryParse(xmlValue, out Date)) return false;
                    break;
                }
                case XmlValueType.Mrn:
                {
                    break;
                }
                case XmlValueType.Uc:
                {
                    break;
                }
                case XmlValueType.Code:
                {
                    break;
                }
                case XmlValueType.Desc:
                {
                    break;
                }
                case XmlValueType.Invoice:
                {
                    break;
                }
                case XmlValueType.Name:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(xmlValueType), xmlValueType, null);
            }
            return true;
        }

        /// <summary>
        ///     Funkcja odpowiada za wczytanie MRNa z pliku IE514
        /// </summary>
        /// <returns></returns>
        public string Read514File()
        {
            var tag = GetTag("WniosekOUniewaznienie");
            Mrn = tag.Attribute("MRN")?.Value;
            return Mrn;
        }

        /// <summary>
        ///     Funckja odpowiada za przeczytanie pliku IE529 i zwrócenie informacji o tym, czy udało się to zrobić
        /// </summary>
        /// <returns></returns>
        public bool Read529File()
        {
            // Funkcja czytać będzie kolejne wartości z pliku XML, a następnie sprawdzać, czy są poprawne.
            // Jeśli nie, to będzie to odnotowywać wartością false przy wpisywaniu do listy Values oraz ustawiać
            // return funkcji na false

            // Data
            var tag = GetTag("Zwolnienie");
            var stringDate = tag.Attribute("DataPrzyjecia")?.Value;
            var ret = true;
            if (!CheckXmlValue(stringDate, XmlValueType.Date))
            {
                Values[XmlValueType.Date].Add(new XmlValue(false, stringDate));
                ret = false;
            }
            else
            {
                Values[XmlValueType.Date].Add(new XmlValue(true, stringDate));
            }
            StringDate = stringDate;
            Date = DateTime.Parse(stringDate).Date;
            // MRN
            Mrn = tag.Attribute("MRN")?.Value;
            if (!CheckXmlValue(Mrn, XmlValueType.Mrn))
            {
                Values[XmlValueType.Mrn].Add(new XmlValue(false, Mrn));
                ret = false;
            }
            else
            {
                Values[XmlValueType.Mrn].Add(new XmlValue(true, Mrn));
            }
            // UCWywozu
            Uc = tag.Attribute("UCWywozu")?.Value;
            if (!CheckXmlValue(Uc, XmlValueType.Uc))
            {
                Values[XmlValueType.Uc].Add(new XmlValue(false, Uc));
                ret = false;
            }
            else
            {
                Values[XmlValueType.Uc].Add(new XmlValue(true, Uc));
            }
            // Kod
            tag = GetTag("Towar");
            Code = tag.Attribute("KodTowarowy")?.Value;
            if (!CheckXmlValue(Code, XmlValueType.Code))
            {
                Values[XmlValueType.Code].Add(new XmlValue(false, Code));
                ret = false;
            }
            else
            {
                Values[XmlValueType.Code].Add(new XmlValue(true, Code));
            }
            // Typ
            Type = tag.Attribute("OpisTowaru")?.Value;
            if (!CheckXmlValue(Type, XmlValueType.Desc))
            {
                Values[XmlValueType.Desc].Add(new XmlValue(false, Type));
                ret = false;
            }
            else
            {
                Values[XmlValueType.Desc].Add(new XmlValue(true, Type));
            }
            // Numery faktur
            foreach (var invoice in GetInvoices("DokumentWymagany"))
            {
                var val = invoice.Attribute("Nr")?.Value;
                if (!CheckXmlValue(val, XmlValueType.Invoice))
                {
                    Values[XmlValueType.Invoice].Add(new XmlValue(false, val));
                    ret = false;
                }
                else
                {
                    Values[XmlValueType.Invoice].Add(new XmlValue(true, val));
                }
                InvoiceNumbers.Add(val);
            }
            Values[XmlValueType.Name].Add(new XmlValue(true, FileName));
            Values[XmlValueType.UniqueId].Add(new XmlValue(true, UniqueId));
            return ret;
        }
    }
}