﻿namespace KronoXMLtoExcel
{
    public class CellCoordinates
    {
        // Koordynaty w tabeli. Będą służyć do kolorowania niepoprawnych komórek
        public int RowIndex;

        public int ColIndex;

        public CellCoordinates()
        {
        }

        public CellCoordinates(int rowIndex, int colIndex)
        {
            RowIndex = rowIndex;
            ColIndex = colIndex;
        }
    }
}